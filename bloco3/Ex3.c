/*
Escreva um programa que contenha duas variaveis inteiras. Leia essas variaveis do 
teclado. Em seguida, compare seus endereços e exiba o conteudo do
maior endereço.
*/
#include <stdio.h>

int main(){
int *pA, *pB, a, b;

pA = &a;
pB = &b;

printf ("Digite A: ");
scanf ("%d", &a);

printf ("Digite B: ");
scanf ("%d", &b);


if (pA > pB){
    printf ("pA contem o maior endereço. Valor: %d", *pA);
} else {
    printf ("pB contem o maior endereço. Valor: %d", *pB);
}

return 0;
}
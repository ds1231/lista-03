/*
2. Escreva um programa que contenha duas variáveis inteiras.
Compare seus endereços e exiba o maior endereço.
*/
#include <stdio.h>

int main(){
int *pA, *pB, a, b;
pA = &a;
pB = &b;

if (pA > pB) {
    printf ("pA é maior que pB: %p\n", pA);
} else {
    printf ("pB é maior que pA: %p\n", pB);
}

return 0;
}
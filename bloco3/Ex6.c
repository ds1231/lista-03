/*
6. Elaborar um programa que leia dois valores inteiros (A e B).
Em seguida faça uma função que retorne a soma do dobro dos dois
números lidos. A função deverá armazenar o dobro de A na própria variável
A e o dobro de B na própria variável B.
*/
#include <stdio.h>

int somaDobro(int *num1, int *num2){
int soma;
    *num1 = *num1 * 2;
    *num2 = *num2 * 2;
    soma = *num1 + *num2;
return soma;
}

int main () {
int a, b, *pA, *pB;

pA = &a;
pB = &b;

printf ("Digite A: ");
scanf ("%d", &a);

printf ("Digite B: ");
scanf ("%d", &b);

printf ("Dobro de A: %d\nDobro de B: %d\nSoma dos dobros: %d\n", a, b, somaDobro (pA, pB));
return 0;
}
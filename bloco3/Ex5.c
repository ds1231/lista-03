/*
5. Faça um programa que leia dois valores inteiros e chame uma função
que receba estes 2 valores de entrada e retorne o maior valor
na primeira variável e o menor valor na segunda variável.
Escreva o conteúdo das 2 variáveis na tela.
*/


#include <stdio.h>


int maiorValor (int *num1, int *num2) {
int aux;
    if (*num2 > *num1) {
        aux = *num2;
        *num2 = *num1;
        *num1 = aux;
     }
return 0;
}


int main(){
int a, b, *pA, *pB;
pA = &a;
pB = &b;
printf ("Digite A: ");
scanf ("%d", &a);
printf ("Digite B: ");
scanf ("%d", &b);

maiorValor (pA, pB);

printf ("Maior: %d\n", *pA);
printf ("Menor: %d\n", *pB);

return 0;
}
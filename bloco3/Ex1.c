/*
1. Escreva um programa que declare um inteiro, um real e um char, e
ponteiros para inteiro, real e char. Associe as variaveis aos ponteiros
(use &). Modifique os valores de cada variável usando os ponteiros.
Imprima os valores das variáveis antes e após da modificação.
*/

#include <stdio.h>

int main () {

int num = 1;
float real = 0.1;
char carac = 'a';

int *pNum;
float *pReal;
char *pCarac;

pNum = &num;
pReal = &real;
pCarac = &carac;

printf ("Antes da modificacao:\n");
printf ("%d\n", *pNum);
printf ("%.2f\n", *pReal);
printf ("%c\n", *pCarac);

*pNum= 2.0;
*pReal = 0.2;
*pCarac = 'b';


printf ("Depois da modificacao:\n");
printf ("%d\n", *pNum);
printf ("%f\n", *pReal);
printf ("%c\n", *pCarac);

return 0;
}
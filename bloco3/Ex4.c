/*
4. Faça um programa que leia 2 valores inteiros e chame uma
função que receba estas 2 variaveis e troque o seu conteudo,
ou seja, esta função e chamada passando duas variaveis A e B
por exemplo e, apos a execução da função, A conterá o valor de B
e B terá o valor de A.
*/

#include <stdio.h>

int trocaVariaveis(int *num1, int *num2){
int aux;

aux = *num1;
*num1 = *num2;
*num2 = aux;

return 0;
}

int main(){
int a, b, *pA, *pB;
pA = &a;
pB = &b;

printf ("Digite o primeiro valor: ");
scanf ("%d", &a);

printf ("Digite o segundo valor: ");
scanf ("%d", &b);

trocaVariaveis(pA, pB);

printf ("A: %d\n", a);
printf ("B: %d\n", b);
return 0;
}
/*
7. Faca uma funcao que receba uma temperatura em graus Celsius e retorne-a convertida 
em graus Farenheit. A fórmula de conversão é: F = C * (9.0/5.0) + 32, sendo
F a temperatura em Fahrenheit e Ca temperatura em Celsius.
*/
#include <stdio.h>

int conversor (float temp) {
temp = temp * (9.0/5.0) + 32;
printf ("A temperatura em Farenheit é: %.2f", temp);
}

int main () {
float celsius;

printf ("Digite uma temperatura em graus Celsius: ");
scanf ("%f", &celsius);

conversor (celsius);

return 0;
}
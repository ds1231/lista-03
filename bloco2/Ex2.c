/*
2. Faça uma funcao que receba a data atual (dia, mes e ano em inteiro) e exiba-a na tela
no formato textual por extenso. Exemplo: Data 01/01/2000, imprimir 1 de janeiro de 2000.
*/
#include <stdio.h>
#include <string.h>

struct month {
    char mes[12];
};

struct month meses [12];

int main () {

strcpy(meses[0].mes,"de Janeiro");
strcpy(meses[1].mes,"de Fevereiro");
strcpy(meses[2].mes,"de Março");
strcpy(meses[3].mes,"de Abril");
strcpy(meses[4].mes,"de Maio");
strcpy(meses[5].mes,"de Junho");
strcpy(meses[6].mes,"de Julho");
strcpy(meses[7].mes,"de Agosto");
strcpy(meses[8].mes,"de Setembro");
strcpy(meses[9].mes,"de Outubro");
strcpy(meses[10].mes,"de Novembro");
strcpy(meses[11].mes,"de Dezembro");

int dia, numMes, ano;

printf ("Digite o dia: ");
scanf ("%d", &dia);

while (1) {
printf ("Digite o mes: ");
scanf ("%d", &numMes);
    if (numMes > 0 && numMes <=12) {
        break;
    } else {
        printf ("Mês invalido.\n");
    }
}

printf ("Digite o ano: ");
scanf ("%d", &ano);

printf ("%d %s de %d", dia, meses[numMes-1].mes, ano);

return 0;
}
/*
Ex 3. Faça uma função para verificar se um número é positivo ou negativo. Sendo que o valor de
retorno sera 1 se positivo, -1 se negativo e 0 se for igual a 0.
*/

int posOuNeg(int num){
    if (num > 0) {
    return 1;
    } else {
    return 0;
    }
}

int main () {
int num;

printf ("Digite o número: ");
scanf ("%d", &num);

printf ("%d\n", posOuNeg (num));
return 0;
}
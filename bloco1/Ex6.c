/*
6. Faça um programa que realize a leitura dos seguintes dados relativos a um conjunto de alunos:
Matricula, Nome, Codigo da disciplina, Nota1 e Nota2. Considere uma turma de até 10 alunos.
Após ler todos os dados digitados, e depois de armazená-los em um vetor de estrutura, exibir na tela
a listagem final dos alunos com as suas respectivas médias finais
(use uma média ponderada: Nota1 com peso 1.0 e Nota2 com peso = 2.0).
*/

#include <stdio.h>

int i, j;
float media;
char c;
struct estudante {
int matricula;
char nome [50];
int codDisciplina;
int nota1, nota2;
};
struct estudante alunos [10];

int main () {

for (i=0; i<10; i++) {
printf ("\nEstudante número %d \n\n", i+1);

        printf ("Digite a matricula: ");
        scanf ("%d", &alunos[i].matricula);
        getchar( );

        printf ("Digite o nome: ");
        fgets (alunos[i].nome, sizeof(alunos[i].nome), stdin);
            for (j=0; j<50; j++) {
            c = alunos[i].nome[j];
                if (c == '\n') {
                    alunos[i].nome[j] = '\0';
                    break;
                }
            }

        printf ("Digite o codigo da disciplina: ");
        scanf ("%d", &alunos[i].codDisciplina);
        
        printf ("Digite a primeira nota: ");
        scanf ("%d", &alunos[i].nota1);
        
        printf ("Digite a segunda nota: ");
        scanf ("%d", &alunos[i].nota2);
        
    }

printf ("\n Coleta de dados concluída. \n\n", i+1);
for (i=0; i<10; i++) {
printf ("%s, ", alunos[i].nome);

media = (alunos[i].nota1 + alunos[i].nota2 * 2) / 2;
printf ("media: %.2f\n", media);

}

return 0;
}
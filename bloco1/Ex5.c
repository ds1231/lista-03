/*
5. Considerando a estrutura

struct Vetor {
    float x;
    float y;
    float z;
};

para representar um vetor no R^3, implemente um programa que calcule a soma de dois vetores.
*/

#include <stdio.h>

struct Vetor {
    float x;
    float y;
    float z;
};

struct Vetor a, b, c;

int main () {

printf ("Digite o 1º valor do 1º vetor: ");
scanf ("%f", &a.x);
printf ("Digite o 2º valor do 1º vetor: ");
scanf ("%f", &a.y);
printf ("Digite o 3º valor do 1º vetor: ");
scanf ("%f", &a.z);

printf ("Digite o 1º valor do 2º vetor: ");
scanf ("%f", &b.x);
printf ("Digite o 2º valor do 2º vetor: ");
scanf ("%f", &b.y);
printf ("Digite o 3º valor do 2º vetor: ");
scanf ("%f", &b.z);

c.x = a.x + b.x;
c.y = a.y + b.y;
c.z = a.z + b.z;

printf ("Resultados da soma:\n");
printf ("x = %.2f\n", c.x);
printf ("y = %.2f\n", c.y);
printf ("z = %.2f\n", c.z);
return 0;
}
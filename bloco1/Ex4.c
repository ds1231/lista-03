/*
4. Crie uma estrutura representando os alunos de um determinado curso. A estrutura deve conter
a matrícula do aluno, nome, nota da primeira prova, nota da segunda prova, e nota da terceira prova.

(a) Permita ao usuário entrar com os dados de 5 alunos.
(b) Encontre o aluno com maior nota da primeira nota.
(c) Encontre o aluno com menor média geral.
(d) Encontre o aluno com menor média geral.
(e) Para cada aluno diga se ele foi aprovado ou reprovado, considerando o valor 6 para aprovação.
*/

#include <stdio.h>

int i, j, posAlunos;
float media, numPedido;
char c;
struct estudantes {
    int matricula;
    char nome [50];
    int nota1, nota2, nota3;
};

struct estudantes alunos [5];
int main () {

for (i=0; i<5; i++) {

printf ("\nEstudante número %d \n\n", i+1);

    printf ("Digite a matricula: ");
    scanf ("%d", &alunos[i].matricula);

    getchar ();

    printf ("Digite o nome: ");
    fgets (alunos[i].nome, sizeof(alunos[i].nome), stdin);
    
    for (j=0; j<50; j++) {
    c = alunos[i].nome[j];
        if (c == '\n') {
            alunos[i].nome[j] = '\0';
            break;
        }
    }

    printf ("Digite a nota da primeira nota: ");
    scanf ("%d", &alunos[i].nota1);

    printf ("Digite a nota da segunda nota: ");
    scanf ("%d", &alunos[i].nota2);

    printf ("Digite a nota da terceira nota: ");
    scanf ("%d", &alunos[i].nota3);
}

printf ("\nDados recolhidos. \n\n", i+1);

numPedido = alunos[0].nota1;
for (i=0; i<5; i++) {
    if (alunos[i].nota1 > numPedido) {
        posAlunos = i;
        numPedido = alunos[i].nota1;
    }
}
printf ("A maior nota da primeira prova é de %s, com %.0f pontos\n\n", alunos[posAlunos].nome, numPedido);

numPedido = (alunos[0].nota1 + alunos[0].nota2 + alunos[0].nota3) / 3;
    for (i=1; i<5; i++) {
    media = (alunos[i].nota1 + alunos[i].nota2 + alunos[i].nota3) / 3;
        if (media>numPedido) {
            posAlunos = i;
            numPedido = media;
        }
    }
printf ("A maior média é de %s: %.2f\n", alunos[posAlunos].nome, numPedido);

posAlunos = 0;
numPedido = (alunos[0].nota1 + alunos[0].nota2 + alunos[0].nota3) / 3;
    for (i=1; i<5; i++) {
    media = (alunos[i].nota1 + alunos[i].nota2 + alunos[i].nota3) / 3;
        if (media<numPedido) {
            posAlunos = i;
            numPedido = media;
        }      
    }

printf ("A menor média é de %s: %.2f\n\n", alunos[posAlunos].nome, numPedido);

for (i=0; i<5; i++) {
    media = (alunos[i].nota1 + alunos[i].nota2 + alunos[i].nota3) / 3;

    if (media > 6) {
        printf ("%s foi aprovado.\n", alunos[i].nome);
    } else {
        printf ("%s foi reprovado.\n", alunos[i].nome);
    }
}

return 0;
}